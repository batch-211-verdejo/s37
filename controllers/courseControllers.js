const Course = require("../models/Course.js");
const User = require("../models/User.js");

// Create a new course
/*
    Steps:
    1. Create a new course object using the mongoose model and the information from the reqBody and the id from the header
    2. Save the new User to the database
*/
module.exports.addCourse = (data) => {
    let newCourse = new Course ({
        name: data.course.name,
        description: data.course.description,
        price: data.course.price
        });
    
    console.log(data);

    if (data.isAdmin) {

        console.log(newCourse);

        return newCourse.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
            });
    } else {
        return false;
    };
};

/* module.exports.addCourse = (data) => {

    let newCourse = new Course ({
            name: data.course.name,
            description: data.course.description,
            price: data.course.price
        });
    
    console.log(data);

    if (!data.isAdmin) {

        return false;

    } else {

        return newCourse.save().then((course, error) =>{
            if (error) {
                return false;
            } else {
                return true;
            }
        });
    };
}; */

// Retrieve all courses
/* 
    1. Retrieve all the courses from the database
        Model.find({})

*/

module.exports.getAllCourses = () =>{
    return Course.find({}).then(result => {
        return result;
    });
};

// Retrieve all Active Courses
/* 
    1. Retireve all the courses from the database with the property of "isActive" to true

*/
module.exports.getAllActive = () =>{
    return Course.find({isActive: true}).then(result => {
        return result;
    });
};


// Retrieving a specific course
/* 
    1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse = (reqParams) => {
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    });
};


// Update a course
/* 
    1. Create a variable "updatedCourse" which will contain the information retrieved from the request body.
    2. Find and update the course using the course ID retrieved from the request params property and the variable "updatedCourse" containing the information from the request body.
*/

module.exports.updateCourse = (reqParams, reqBody) => {
    let updatedCourse = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    };
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) =>{
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

// Archive a course (s40 activity)
module.exports.archiveCourse = (reqParams, reqBody) => {
    let archivedCourse = {        
        isActive: reqBody.isActive
    };
    return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) =>{
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};

// Solution # 2
/* module.exports.archiveCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams, {isActive: false}).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
}; */

// Stretch goal
/* module.exports.activateCourse = (reqParams, reqBody) => {
    let activatedCourse = {
        isActive: reqBody.isActive
    };
    return Course.findByIdAndUpdate(reqParams.courseId, activatedCourse).then((course, error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
}; */

module.exports.activateCourse = (reqParams) => {
    return Course.findByIdAndUpdate(reqParams, {isActive: true}).then((course,error) => {
        if (error) {
            return false;
        } else {
            return true;
        };
    });
};
// s40 activity end














/* module.exports.courseLists = (reqBody) => {
    return Course.find()
    .then(result => {
        return result;
    })
} */