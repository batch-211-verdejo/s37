// s38
const User = require("../models/User.js");
const Course = require("../models/Course.js");

// bcrypt is a package which allows us to hash our password to add a layer of security for our user's details. <password encryption>
const bcrypt = require("bcrypt");

const auth = require("../auth.js");

// Check if the email already exists
/* 
    Steps
    1. Use mongoose "find" method to find duplicate emails
    2. Use the "then" method to send a response back to the FE application based on the result of the "find" method

*/

module.exports.checkEmailExists = (reqBody) => {
    return User.find({email: reqBody.email})
    .then(result => {
        if (result.length > 0) {
            return true;
        } else {
            return false;
        };
    });
};

// User Registration
/* 
    Steps
    1. Create a new User object using the mongoose models and the info from the reqBody
    2. Make sure that the password is encrypted
    3. Save the new user to the database
*/

module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNumber: reqBody.mobileNumber,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {            
            return true;
        };
    })
};

// User Authentication
/* 
    Steps:
    1. Check the database if the user email exists
    2. Compare the password in the login form with the password stored in the database.
    3. Generate/return a JSON Web token if the user is successfully logged in and return false if not.

*/

module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email})
    .then(result => {
        if (result == null) {
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result)}
            } else {
                return false;
            };
        };
    });
};

// s38 getProfile controller method for retrieving the details of the user
/* module.exports.getProfile = (reqBody) => {
    return User.findById(reqBody.id).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }
        result.password = "";
        return result
    });
}; */

module.exports.getProfile = (data) => {
    return User.findById(data.userId)
    .then(result => {
        result.password = "";
        return result;
    });
};

// Enroll user to a class
/* 
    Steps:
    1. Find the document in the database using the user's ID
    2. Add the courseID to the user's enrollment array
    3. Update the document in MongoDB

*/

/* 
    First, find the user who is enrolling and update his enrollemnts subdocument array. We will push the courseID in the enrollment array.

    Second, find the course where we are enrolling and update its enrollees. We will push the userId in the enrollees subdocument array.

    Since we will access 2 collections in one action -- we will have to wait for the completion of the action instead of letting Javascript continue line per line.

    To be able to wait for the result of a function, we use the await keyword. The await keyword allows us to wait for the function to finish and get a result before proceeding.

*/

module.exports.enroll = async (data) => {

    let isUserUpdated = User.findById(data.userId).then(user => {

        user.enrollments.push({courseId: data.courseId});

        return user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });
    });

    let isCourseUpdated = Course.findById(data.courseId).then(course => {

        course.enrollees.push({userId: data.userId});

        return course.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            };
        });
    });

    if (isUserUpdated && isCourseUpdated) {
        return true;
    } else {
        return false;
    }
};
















// Find user credentials through email
module.exports.emailSearch = (data) => {
    return User.findOne(data.email).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        }        
        return result
    });
};

module.exports.getAllUser = () => {
    return User.find({}).then(result => {
        return result;
    })
};
