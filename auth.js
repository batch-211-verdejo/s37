/* 
    auth.js is our own module which will contain methods to help authorize or restrict users from accessing certain features in our application

*/

const jwt = require("jsonwebtoken");
const secret = "CoursebookingAPI";

// JSON Web Tokens
/* 
    - JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of the server
*/

module.exports.createAccessToken = (user) => {
    const data = {
        id : user._id,
        email :  user.email,
        isAdmin : user.isAdmin
    };
    return jwt.sign(data, secret, {});
};

// s39 Toke Verification
/* 
    Receive the gift and open the lock to verify if the sender is legitimate and the gift was not tampered with
*/

module.exports.verify = (req, res, next) => {
    // the token is retrieved from the request header
    // this can be provided in postman under
        // Authorization > Bearer token
    let token = req.headers.authorization;

    if (typeof token !== "undefined") {
        console.log(token);
        token = token.slice(7, token.length);
        // the "slice" method takes only the token from the info sent via request header
        // Bearer y1dojfddpofk
        return jwt.verify(token, secret, (err, data) =>{
            if (err) {
                return res.send({auth: "failed"})
            } else {
                // allows the app to proceed with the next middleware function/callback function
                next();
            }
        })
    } else {
        return res.send({auth: "failed"});
    };
};

// Token decryption
/*
    Open the gift and get the content
*/

module.exports.decode = (token) => {
    if(typeof token !== "undefined") {
        token = token.slice(7, token.length);
        return jwt.verify(token, secret, (err, data) =>{
            if (err) {
                return null
            } else {
                return jwt.decode(token, {complete: true}).payload;
            }
        })
    } else {
        return null;
    };
};

/* 
    create a courseRoutes.js
    import:
    express
    router
    courseController.js

    create a courseControllers.js
    import
    course model
*/