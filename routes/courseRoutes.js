const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers.js");
const auth = require("../auth.js");


// Add a new course
router.post("/", auth.verify, (req, res) => {
    
    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    console.log(data);
    // data.course.name
    courseController.addCourse(data)
    .then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the courses
router.get("/all", (req,res) => {
    courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});

// Route for retrieving all active courses
router.get("/", (req,res) => {
    courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving a specific course
router.get("/:courseId", (req,res) => {
    courseController.getCourse(req.params).then(resultFromController =>res.send(resultFromController));
});

// Route for updating a course
router.put("/:courseId", auth.verify, (req, res) => {
    courseController.updateCourse(req.params, req.body).then(resultFromController =>res.send(resultFromController));
});

// Route for archiving a course (s40 activity)
router.put("/:courseId/archive", auth.verify, (req,res) => {
    courseController.archiveCourse(req.params, req.body)
    .then(resultFromController => res.send(resultFromController));
});

router.put("/:courseId/activate", auth.verify, (req,res) => {
    courseController.activateCourse(req.params, req.body)
    .then(resultFromController => res.send(resultFromController));
});



























router.get("/", (req, res) => {    
    courseController.courseLists(req.body)
    .then(resultFromController => res.send(resultFromController))
});
























module.exports = router;