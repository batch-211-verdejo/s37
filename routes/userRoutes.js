const express = require("express");
const router = express.Router();

const userController = require("../controllers/userController.js");
const auth = require("../auth.js");


// Route for checking if the user's email already exists in the database
// Pass the "body" property of our request

router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body)
    .then(resultFromController => res.send(resultFromController));
});

// Route for user registration
router.post("/register", (req,res) => {
    userController.registerUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body)
    .then(resultFromController => res.send(resultFromController));
});


// s38 route that will accept the user's ID to retrieve the details of a user

router.get("/details", auth.verify, (req, res) =>{
    
    const userData = auth.decode(req.headers.authorization);
    
    userController.getProfile({userId: userData.id})
    .then(resultFromController => res.send(resultFromController));
});

// Route to enroll a user to a specific course with authentication
router.post("/enroll", auth.verify, (req, res) => {    

    let data = {
        userId: auth.decode(req.headers.authorization).id,
        courseId: req.body.courseId        
    }

    userController.enroll(data).then(resultFromController => res.send(resultFromController));
});





















// Find user credentials through email
router.post("/credentials", (req, res) => {
    userController.emailSearch({email: req.body.email})
    .then(resultFromController => res.send(resultFromController));
});

router.get("/", (req, res) => {
    userController.getAllUser()
    .then(resultFromController => res.send(resultFromController));
});



module.exports = router;



















