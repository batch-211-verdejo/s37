// Booking System MVP Requirements
/* 
    Minimum Viable Product is a product that has enough features to be useful to its target market. It is used to validate a product idea at the onset of development.
*/
/* 
    User Registration
    User authentication (login)
    Retrieval of authenticated user's
 */

// Booking System API Dependencies
/* 
    Besides express and mongoose, we have the following additional packages:
        bcrypt - for hashing user passwords prior to storage
        cors - for allowing cross-origin resource sharing
*/

// First thing to do when creating app
/* 
    Gather information
    Planning
    Framework

*/


// s38 Activity
/* 
    1. Create a "/details" route that will accept the user's Id to retrieve the details of a user.
    2. Create a getProfile controller method for retrieving the details of the user.
    3. Find the document in the database using the user's ID.
    4. Reassign the password of the returned document to an empty string (" ").
    5. Return the result back to the frontend/postman.
    6. Process a POST request at the "/details" route using postman to retrieve the details of the user.
*/

// s39 Activity
/*    
    Refactor the addCourse controller method to implement admin authentication for creating a course.
    Steps:
    1. Create a conditional statement that will check if the user is an administrator.
    2. Create a new Course object using the mongoose model and the information from the request body and the id from the header
    3. Save the new User to the database
    Pushing instructions    

Add the link in BooDLE
Send a screenshot
*/

// s40 Activity
/* 
    1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the URL.
    2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
    3. Process a PUT request at the /courseId/archive route using postman to archive a course
*/

// s41 Activity
/*
    1. Refactor the user route to implement user authentication for the enroll route
    2. Process a POST request at the /enroll route using postman to enroll a user to a course    
*/

/* 
    Render.com API deployment
        1. web services
        2. connect to repository
        3. insert name (no spaces, use "-" instead)
        4. Build command field type npm install
        5. Start command field type node index.js(your server file)
        6. Choose plan and click "Create Web Services"
*/
