const express = require("express");
const mongoose = require("mongoose");
// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require("cors");

const userRoutes = require("./routes/userRoutes.js"); //s38
const courseRoutes = require("./routes/courseRoutes.js"); //s39

const app = express();

mongoose.connect("mongodb+srv://admin123:admin123@project0.zzkxx4f.mongodb.net/s37-s41?retryWrites=true&w=majority", 
    {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }
);

mongoose.connection.once("open", () => console.log("Connected to MongoDB Atlas!"))

// Allows all resources to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/users", userRoutes); //s38
app.use("/courses", courseRoutes); //s39

app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});